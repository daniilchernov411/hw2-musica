import React from "react";
import style from "./CardList.module.css";
import PropTypes from "prop-types";
import Card from "../Card/Card";

export default class CardList extends React.Component {

	state = {
		cards: JSON.parse(localStorage.getItem("cards")) || [],
		favorites: JSON.parse(localStorage.getItem("favorites")) || [],
	};

	addtoCart = (code) => {
		const cards = [...this.state.cards, code];
		this.setState({ cards: cards });
		localStorage.setItem("cards", JSON.stringify(cards));
	}

	selecFavorite = (isFavorites) => {
		this.setState({ isFavorites: isFavorites });
		localStorage.setItem("isFavorites", JSON.stringify(isFavorites));
	}

	render() {
		return (
			<div className={`${style.wrapper} container`}>
				{this.props.productList.map((product) => {
					return (
						<Card
							key={product.id}
							urlImg={product.imgUrl}
							alt={product.productName}
							productName={product.productName}
							code={product.vendorСod}
							price={product.price}
							infoTitle={product.productName}
							addToCart={this.addtoCart}
							favoritesArr={this.state.favorites}
							onClickSetFavorites={this.selecFavorite}
						/>
					);
				})}
			</div>
		);
	}
}

Card.propTypes = {
	productList: PropTypes.array,
  };


  