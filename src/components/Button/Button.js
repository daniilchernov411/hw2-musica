import React from 'react';
import PropTypes from 'prop-types';

export default class Button extends React.Component {
    render() {
        return (
            <button
                onClick={this.props.click}
                className={`${this.props.size}  ${this.props.color} waves-effect btn`}
            >
                {this.props.icon}
                {this.props.text}
            </button>
        );
    }
}

Button.PropTypes = {
    click: PropTypes.func,
    size: PropTypes.string,
    color: PropTypes.string,
    icon: PropTypes.object,
    text: PropTypes.string,
};

Button.defaultProps = {
    icon: null,
    color: "#ff9100",
}
