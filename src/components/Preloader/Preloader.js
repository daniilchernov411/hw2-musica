import React from "react";
import styl from "./Preloader.module.css";

export default class Preloader extends React.Component {
  render() {
    return (
      <div className={styl.preloader}>
        <div className={styl.loader}></div>
      </div>
    );
  }
}
