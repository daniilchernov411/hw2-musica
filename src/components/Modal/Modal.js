import React from "react";
import styles from "./Modal.module.css";
import PropTypes from "prop-types";
import Button from "../Button/Button";

export default class Modal extends React.Component {
    render() {
        return (
            <div
                onClick={this.props.isOpen} className={styles.modal}>
                <div
                    onClick={(event) => event.stopPropagation()}
                    className={styles.modal__contant}
                >
                    <div
                        className={styles.modal__header}>
                        <h3 className={styles.modal__title}>{this.props.header}</h3>
                        {this.props.closeButton && <span onClick={this.props.isOpen}>&#10761;</span>}
                    </div>
                    <div
                        className={styles.modal__nav}>
                        <p className={styles.modal__text}>{this.props.modaltext}</p>
                    </div>
                    {this.props.active}
                </div>
            </div>
        );
    }
}

Button.PropTypes={
    isOpen: PropTypes.func,
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    modaltext: PropTypes.string,
    active:PropTypes.object
}

Button.defaultProps = {
    closeButton: true,
  };