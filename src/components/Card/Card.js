import React from "react";
import styles from './Card.module.css';
import PropTypes from "prop-types";
import Button from '../Button/Button';
import Modal from "../Modal/Modal";
import Favorites from "../Favorites/Favorites";

export default class Card extends React.Component {

    state = {
        modal: false,
    }

    modalActive = () => {
        this.setState({ modal: !this.state.modal })
    };
    render() {
        const { urlImg, alt, productName, code, price, infoTitle, onClickSetFavorites, favoritesArr, addToCart } = this.props;
        return (
            <>
                <div className={`${styles.card} card hoverable`}>

                    <div className="card-image waves-effect waves-block waves-light">
                        <h5
                            className={`${styles.code} grey-text text-darken-4 right`}>
                            {`code: ${code}`}
                        </h5>
                        <img className={`${styles.img} activator`} src={urlImg} alt={alt} />
                    </div>

                    <div className={`${styles.card_content} card-content`}>
                        <span className={`${styles.card_title} card-title activator grey-text text-darken-4`}>
                            <h5 className={styles.cardTitle}>{productName}</h5>
                            <i className="material-icons right">more_vert</i>
                        </span>
                        <h4
                            className={`${styles.price} grey-text text-darken-4`}>
                            {`\u20B4 ${new Intl.NumberFormat("ua-Ua").format(price)}`}
                        </h4>
                        <p className={styles.footer}>
                            <Button
                                click={this.modalActive}
                                color={"orange accent-3"}
                                icon={<i className="material-icons left">shopping_cart</i>}
                                text={"Добавить в корзину"}
                            />
                            <Favorites
                                favoritesArr={favoritesArr}
                                onClickSetFavorites={onClickSetFavorites}
                                code={code}
                            />
                        </p>
                    </div>

                    <div className="card-reveal">
                        <span className="card-title grey-text text-darken-4">
                            {infoTitle}
                            <i className="material-icons right">close</i>
                        </span>
                        <p>
                            Медицинская техника — это набор технических устройств, которые применяются в медицине для лечения, диагностирования, профилактики различных болезней, для изготовления лекарств и проведения мероприятий санитарно-гигиенического характера.
                        </p>
                        <p>
                            Первые подобные устройства возникли очень давно и постепенно совершенствовались в процессе развития разных отраслей клинической медицины. На сегодняшний день всю медицинскую технику можно разделить на несколько категорий в зависимости от ее функционального назначения. Это: медицинские приборы, инструменты, аппараты, оборудование и расходные вещества.
                        </p>
                        <p>
                            Инструменты представляют собой технические приспособления, с помощью которых осуществляются лечебные, диагностические и профилактические процедуры. Они предназначены для держания в руке и активации с помощью силы мышц, а также могут быть сменным элементом медицинского аппарата.
                        </p>
                        <p>
                            Медицинские приборы являются устройствами, которые позволяют проанализировать состояние организма и вынести диагноз.
                        </p>
                        <p>
                            Оборудование — медицинские технические устройства, с помощью которых создаются оптимальные условия для пациента и медицинских работников для осуществления лечебно-диагностических мероприятий.
                        </p>
                        <p>
                            Медицинская аппаратура — это совокупность устройств, работа которых заключается в генерации различных видов энергии для влияния на организм. В эту категорию также входят устройства, которые временно заменяют функциональные системы организма. К медицинским аппаратам принадлежат приспособления для приведения в движение инструментов для механического влияния на отдельные части организма.
                        </p>
                    </div>
                </div>
                {this.state.modal && (
                    <Modal
                        isOpen={this.modalActiv}
                        header={"Добавить в корзину"}
                        closeButton={true}
                        textmodal={"Вы уверены, что хотите добавить этот товар в корзину?"}
                        active={
                            <div className={styles.modalFooter}>
                                <Button
                                    size={"l"}
                                    click={() => {
                                        this.modalActive();
                                        addToCart(code);
                                    }}
                                    color={"orange accent-3"}
                                    text={"OK"}
                                />
                                <Button
                                    size={"l"}
                                    click={this.modalActive}
                                    color={"orange accent-3"}
                                    text={"Cancel"}
                                />
                            </div>
                        }
                    />
                )}
            </>
        );
    }

}

Card.propTypes = {
    urlImg: PropTypes.string,
    alt: PropTypes.string,
    productName: PropTypes.string,
    code: PropTypes.number,
    price: PropTypes.number,
    infoTitle: PropTypes.string,
    addToCart: PropTypes.func,
    addFavorit: PropTypes.func,
    removeFavorit: PropTypes.func,
    color: PropTypes.string,
};