import React from "react";

export default class Footer extends React.Component {
  render() {
    return (
      <footer className="page-footer blue-grey darken-3">
        <div className="container">
          <div className="row">
            <div className="col l5 s12 hide-on-small-only">
              <h5 className="white-text">Медицинское оборудование</h5>
            </div>
            <div className="col l4 s12 hide-on-small-only">
              <h5 className="white-text">Новое Медицинское оборудование</h5>
              <ul>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Хирургическое оборудование
                  </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Стоматологическое оборудование
                  </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Физиотерапевтическое оборудование
                  </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    УЗИ аппараты
                  </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Стерилизационное оборудование
                  </a>
                </li>
              </ul>
            </div>
            <div className="col l3 s12">
              <h5 className="white-text">Для клиентов</h5>
              <ul>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Оплата
                  </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Доставка
                  </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Публичная оферта                  
                    </a>
                </li>
                <li>
                  <a className="grey-text text-lighten-3" href="/">
                    Гарантия умови                  
                    </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer-copyright hide-on-small-only">
          <div className="container center">© 2021 Medical Store Project</div>
        </div>
      </footer>
    );
  }
};
