import React from "react";
import PropTypes from "prop-types";
import style from "../Favorites/Favorites.module.css";

export default class Favorites extends React.Component {
    render() {
      const { code, favoritesArr, onClickSetFavorites } = this.props;
      const isFavorites = favoritesArr.includes(code);
      return (
        <span className={style.favorite}>
          {isFavorites ? (
            <i
              className={`${style.favorite__icon} material-icons`}
              onClick={() => {
                onClickSetFavorites(favoritesArr.filter((n) => n !== code));
              }}
            >
              favorite
            </i>
          ) : (
            <i
              className={`${style.favorite__icon_sel} material-icons`}
              onClick={() => {
                onClickSetFavorites([...favoritesArr, code]);
              }}
            >
              favorite_border
            </i>
          )}
        </span>
      );
    }
  }
  Favorites.propTypes = {
    code: PropTypes.string,
    favoritesArr: PropTypes.array,
    onClickSetFavorites: PropTypes.func.isRequired,
  };
  
  Favorites.defaultProps = {
    favoritesArr: [],
  };
  