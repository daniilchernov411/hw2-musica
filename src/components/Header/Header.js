import React from "react";
// import styles from "./Header.module.css";

export default class Navbar extends React.Component {
    render() {
        return (
            <nav>
                <div className="nav-wrapper blue-grey darken-3">
                    <div className="container">
                        <a
                            href="#"
                            className="brand-logo"
                            style={{ color: '#00bbff', fontSize: 50, fontWeight: 700 }}
                        >
                            <i
                                className="material-icons"
                                style={{ color: "#00bbff", fontSize: 50 }}
                            >devices_other</i>
                            Медицинский магазин  
                        </a>
                        <ul className="right hide-on-med-and-down">
                            <li>
                                <a
                                    href="#">
                                    <i className="material-icons left">home</i>
                                    Главное
                                </a>
                            </li>
                            <li>
                                <a
                                    href="#">
                                    <i className="material-icons left">favorite_border</i>
                                    Избранное
                                </a>
                            </li>
                            <li>
                                <a
                                    href="#">
                                    <i className="material-icons left">shopping_cart</i>
                                    Корзина
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

