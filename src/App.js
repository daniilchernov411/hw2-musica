import React from "react";
import styles from './App.module.css';
import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize";
import Header from './components/Header/Header';
import CardList from "./components/CardList/CardList";
import Footer from "./components/Footer/Footer";
import Preloader from './components/Preloader/Preloader';

export default class App extends React.Component {

    state = {
        cards: [],
        isLoading: false,
        error: null,
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        fetch("product.json")
            .then((res) => {
                if (res.ok) {
                    return res.json();
                } else {
                    throw new Error("Ошибка загрузки");
                }
            })
            .then((card) => {
                this.setState({ cards: card, isLoading: false })
            })
            .catch((e) => {
                this.setState({ error: e.message, isLoading: false });
            });
    }

    render() {
        const { cards, isLoading, error } = this.state;
        return (
            <div className={styles.App}>
                <header>
                    <Header />
                </header>
                <main className={styles.main}>
                    {isLoading && <Preloader />}
                    {error && <div>{error}</div>}
                    {cards && !cards.length ? (
                        <div>{"Список товаров пустой!!!"}</div>
                    ) : (
                        <CardList productList={cards} />
                    )}
                </main>
                <footer>
                    <Footer />
                </footer>
            </div>
        );
    }
}
